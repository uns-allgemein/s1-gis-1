def FindLabel ( [SHAPE_Area] ):
  # Create float out of string with ","
  number = float([SHAPE_Area].replace(',', '.'))
  # Round float to two digits
  rounded = round(number, 2)
  # Return Labeling-String
  return str(rounded) + u" m²"